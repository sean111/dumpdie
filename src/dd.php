<?php
if(!function_exists('dd')) {
	function dd($arg)
	{
		$args = func_get_args();
		$func = 'var_dump';
		if(function_exists("dump")) {
			$func = 'dump';
		}
		foreach($args as $arg) {
			$func($arg);
		}
		die;
	}
}